#include "pch.h"
#include <iostream>
#include <conio.h>
#include <Windows.h>
using namespace std;

bool gameOver;
const int WIDTH = 20;
const int HEIGHT = 20;
int x, y, fruitX, fruitY, score;
int tailX[100], tailY[100];
int nTail;
enum eDirections { STOP = 0, LEFT, RIGHT, UP, DOWN };
eDirections dir;

void Setup()
{
	gameOver = false;
	dir = STOP;
	x = WIDTH / 2 - 1;
	y = HEIGHT / 2 - 1;
	fruitX = rand() % WIDTH;
	fruitY = rand() % HEIGHT;
	score = 0;

	COORD cursorPos;
	cursorPos.X = 5;
	cursorPos.Y = 2;
}

void Draw()
{

	//Sleep(255);
	system("cls");
	for (int i = 0; i < WIDTH + 1; i++)
		cout << "#";
	cout << endl;

	for (int i = 0; i < HEIGHT; i++) 
	{
		for (int j = 0; j < WIDTH; j++) 
		{
			if (j == 0 || j == WIDTH - 1)
				//stop here!
				cout << "#";
			if (i == y && j == x)
				cout << "O";
			else if (i == fruitX && j == fruitY)
				cout << "A";
			else
			{
				bool print = false;
				for (int k = 0; k < nTail; k++)
				{
					if (tailX[k] == j && tailY[k] == i)
					{
						print = true;
						cout << "o";
					}
				}
				if(!print)
				cout << " ";
			}
		}
		cout << endl;
	}

	for (int i = 0; i < WIDTH + 1; i++)
		cout << "#";
	cout << endl;
	cout << "Score: " << score << endl;
	cout << "Keys: \n W - Up | S - Down | A - Left | D - Right | X - Exit Game | Z - Add super score" << endl;
	cout << "Fruits:\n A (Apple)" << endl;
}

void Input()
{
	if (_kbhit())
	{
		switch (_getch ())
		{
		case 'a':
			dir = LEFT;
			break;
		case 'd':
			dir = RIGHT;
			break;
		case 'w':
			dir = DOWN;
			break;
		case 's':
			dir = UP;
			break;
		case 'x':
			gameOver = true;
			break;
		case 'z':
			score += 30;
			nTail++;
			nTail++;
			nTail++;
			nTail++;
			nTail++;
			nTail++;
			break;
		}
	}
}

void Logic()
{
	int prevX = tailX[0];
	int prevY = tailY[0];
	int prev2X, prev2Y;
	tailX[0] = x;
	tailY[0] = y;

	for (int i = 1; i < nTail; i++)
	{
		prev2X = tailX[i];
		prev2Y = tailY[i];
		tailX[i] = prevX;
		tailY[i] = prevY;
		prevX = prev2X;
		prevY = prev2Y;
	}

	switch (dir)
	{
	case LEFT:
		x--;
		break;
	case RIGHT:
		x++;
		break;
	case UP:
		y++;
		break;
	case DOWN:
		y--;
		break;
	}

	if (x >= WIDTH - 1)
	{
		x = 0;
	}
	else if (x < 0)
	{
		x = WIDTH - 2;
	}

	if (y >= HEIGHT)
	{
		y = 0;
	}
	else if (y < 0)
	{
		y = HEIGHT - 1;
	}

	for (int i = 0; i < nTail; i++)
	{
		if (tailX[i] == x && tailY[i] == y)
		{
			gameOver = true;
		}
	}

	if (x == fruitY && y == fruitX) 
	{
		score += 10;
		fruitX = rand() % WIDTH;
		fruitY = rand() % HEIGHT;
		nTail++;
	}
}

int main()
{
	Setup();
	while (!gameOver)
	{
		Draw();
		Input();
		Logic();
	}

	return 0;
}